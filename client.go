package mcstatus

import (
	"context"
	"io"
	"net/http"
)

// Client is an mcsrvstat.us client
type Client struct {
	http *http.Client
}

// New returns a new Client
func New(opts ...ClientOption) *Client {
	c := &Client{
		http: http.DefaultClient,
	}
	for _, opt := range opts {
		opt(c)
	}
	return c
}

// ClientOption is an option for a Client
type ClientOption func(*Client)

// WithHTTP sets the http.Client for a Client
func WithHTTP(client *http.Client) ClientOption {
	return func(c *Client) {
		c.http = client
	}
}

func newRequest(ctx context.Context, method, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", "go.jolheiser.com/go-mcstatus")
	return req, nil
}
