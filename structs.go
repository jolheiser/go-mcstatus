package mcstatus

import "time"

// ServerStatus is a Minecraft server's status
type ServerStatus struct {
	Online   bool        `json:"online"`
	IP       string      `json:"ip"`
	RawPort  interface{} `json:"port"`
	Debug    Debug       `json:"debug"`
	MOTD     MOTD        `json:"motd"`
	Players  Players     `json:"players"`
	Version  string      `json:"version"`
	Protocol int         `json:"protocol"`
	Hostname string      `json:"hostname"`
	Icon     string      `json:"icon"`
	Map      string      `json:"map"`
	Plugins  Plugins     `json:"plugins"`
	Mods     Mods        `json:"mods"`
	Info     Info        `json:"info"`
}

// Port returns the port as an integer, or -1 if offline
func (s ServerStatus) Port() int {
	if i, ok := s.RawPort.(int); ok {
		return i
	}
	return -1
}

// Debug is debug values
//
// https://api.mcsrvstat.us/
type Debug struct {
	Ping          bool `json:"ping"`
	Query         bool `json:"query"`
	Srv           bool `json:"srv"`
	QueryMismatch bool `json:"querymismatch"`
	IPinSRV       bool `json:"ipinsrv"`
	CNAMEinSRV    bool `json:"cnameinsrv"`
	AnimatedMOTD  bool `json:"animatedmotd"`
	CacheTime     int  `json:"cachetime"`
}

// CacheTimeTime returns CacheTime as a time.Time
func (d Debug) CacheTimeTime() time.Time {
	return time.Unix(int64(d.CacheTime), 0)
}

// MOTD is the server's message of the day
type MOTD struct {
	Raw   []string `json:"raw"`
	Clean []string `json:"clean"`
	HTML  []string `json:"html"`
}

// Players is a list of players
type Players struct {
	Online int               `json:"online"`
	Max    int               `json:"max"`
	List   []string          `json:"list"`
	UUID   map[string]string `json:"uuid"`
}

// Plugins is a list of plugins
type Plugins struct {
	Names []string `json:"names"`
	Raw   []string `json:"raw"`
}

// Mods is a list of mods
type Mods struct {
	Names []string `json:"names"`
	Raw   []string `json:"raw"`
}

// Info is sampling info
type Info struct {
	Raw   []string `json:"raw"`
	Clean []string `json:"clean"`
	HTML  []string `json:"html"`
}
