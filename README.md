# go-mcstatus

[![Go Reference](https://pkg.go.dev/badge/go.jolheiser.com/go-mcstatus.svg)](https://pkg.go.dev/go.jolheiser.com/go-mcstatus)

An API wrapper for https://mcsrvstat.us

## License

[MIT](LICENSE)